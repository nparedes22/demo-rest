package com.demo.falabella.rest.demo_rest.dao;

import com.demo.falabella.rest.demo_rest.models.Cliente;

public interface ClienteDao {

	public boolean insertarCliente(Cliente cliente);
}
