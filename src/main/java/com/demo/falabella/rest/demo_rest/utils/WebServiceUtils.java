package com.demo.falabella.rest.demo_rest.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WebServiceUtils {
	private static final Logger logger = LogManager.getLogger(WebServiceUtils.class);
	public static String consumirRestParaGenerarCliente() {
		try {

            URL url = new URL("https://randomuser.me/api/");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new Exception("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String respuesta;
            while ((respuesta = br.readLine()) != null) {
                return respuesta;
            }
            conn.disconnect();

        } catch (Exception e) {
            logger.error("Exception in NetClientGet:- {0}", e);
        }
		return null;
	}
}
