package com.demo.falabella.rest.demo_rest.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import org.json.JSONArray;
import org.json.JSONObject;

import com.demo.falabella.rest.demo_rest.dao.ClienteDao;
import com.demo.falabella.rest.demo_rest.dao.impl.ClienteDaoImpl;
import com.demo.falabella.rest.demo_rest.models.Cliente;
import com.demo.falabella.rest.demo_rest.service.ClienteService;
import com.demo.falabella.rest.demo_rest.utils.WebServiceUtils;
import com.demo.falabella.rest.demo_rest.utils.QueueUtils;

public class ClienteServiceImpl implements ClienteService {

	public Cliente generarCliente() {
		String result = WebServiceUtils.consumirRestParaGenerarCliente();
		return mapearJsonCliente(result);
	}

	private Cliente mapearJsonCliente(String json) {
		Cliente cliente = new Cliente();
		JSONObject obj = new JSONObject(json);
		JSONArray arr = obj.getJSONArray("results");
		cliente.setGenero(arr.getJSONObject(0).getString("gender"));
		JSONObject objName = arr.getJSONObject(0).getJSONObject("name");
		JSONObject objLocation = arr.getJSONObject(0).getJSONObject("location");
		JSONObject objDob = arr.getJSONObject(0).getJSONObject("dob");
		cliente.setNombre(objName.getString("first"));
		cliente.setApellido(objName.getString("last"));
		cliente.setCiudad(objLocation.getString("city"));
		cliente.setPais(objLocation.getString("country"));
		cliente.setFechaNacimiento(formatearFecha(objDob.getString("date")));
		cliente.setCelular(arr.getJSONObject(0).getString("phone"));
		return cliente;
	}

	private LocalDate formatearFecha(String fechaInstant) {
		Instant instant = Instant.parse(fechaInstant);
		LocalDateTime result = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
		return result.toLocalDate();
	}

	public Boolean encolarCliente(Cliente cliente) {
		return QueueUtils.encolarMensaje(crearMensaje(cliente));
	}

	private String crearMensaje(Cliente cliente) {
		String generoMsn = String.format("%8s", cliente.getGenero());
		String nombreMsn = String.format("%8s", cliente.getNombre());
		String apellidoMsn = String.format("%8s", cliente.getApellido());
		String ciudadMsn = String.format("%8s", cliente.getCiudad());
		String paisMsn = String.format("%8s", cliente.getPais());
		String fechaNacimientoMsn = String.format("%16s", cliente.getFechaNacimiento());
		String celularMsn = String.format("%10s", cliente.getCelular());
		return generoMsn + nombreMsn + apellidoMsn + ciudadMsn + paisMsn + fechaNacimientoMsn + celularMsn;
	}

	public Boolean insertarCliente(Cliente cliente){
		ClienteDao clienteDao = new ClienteDaoImpl();
		return clienteDao.insertarCliente(cliente);
	}

}
